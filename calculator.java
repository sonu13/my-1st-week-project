package com.hcl.calci;

import java.util.Scanner;

public class calculator {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		char operator;
		double num1, num2, result;
		System.out.println("choose operator:+,-,*,/");
		operator = scan.next().charAt(0);
		System.out.println("enter 1st number");
		num1 = scan.nextDouble();
		System.out.println("enter 2nd number");
		num2 = scan.nextDouble();
	
		 if(num2!=0) {

			    switch (operator) {

			      // performs addition between numbers
			      case '+':
			        result = num1 + num2;
			        System.out.println(num1 + " + " + num2 + " = " + result);
			        break;

			      // performs subtraction between numbers
			      case '-':
			        result = num1 - num2;
			        System.out.println(num1 + " - " + num2 + " = " + result);
			        break;

			      // performs multiplication between numbers
			      case '*':
			        result = num1 * num2;
			        System.out.println(num1 + " * " + num2 + " = " + result);
			        break;

			      // performs division between numbers
			      case '/':
			        result = num1 / num2;
			        System.out.println(num1 + " / " + num2 + " = " + result);
			        break;

			      default:
			        System.out.println("You enter wrong option");
			        break;
			    }
			    }
			    else {
			    	System.out.println("Cannot be divided on console");
			    }

			    scan.close();

		
	}

}
