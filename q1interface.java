package com.hcl.q1;


interface common
{
	abstract String markAttendance();
	abstract String dailyTask();
	abstract String displayDetails();
	
}
class Employee implements common
{
	public String markAttendance()
	{
		return "Attendance marked for today";
	}
	public String dailyTask()
	{
		return "complete coding of module1";
	}
	public String displayDetails()
	{
		return "employee details";
	}
}
class Manager implements common
{
	public String markAttendance()
	{
		return "Attendance marked for today";
	}
	public String dailyTask()
	{
		return "create project architecture";
	}
	public String displayDetails()
	{
		return "manager details";
	}
}


public class q1interface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		common c=new Employee();
	
		System.out.println(c.markAttendance());
		System.out.println(c.dailyTask());
		System.out.println(c.displayDetails());
		
		common c1=new Manager();
		System.out.println(c1.markAttendance());
		System.out.println(c1.dailyTask());
		System.out.println(c1.displayDetails());
		


	}

}
